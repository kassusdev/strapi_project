"use strict";

const { parseMultipartData, sanitizeEntity } = require("strapi-utils");

module.exports = {
  /**
   * Retrieve records.
   *
   * @return {Array}
   */

  async find(ctx) {
    let entities;
    if (ctx.query._q) {
      entities = await strapi.services.identity.search(ctx.query);
    } else {
      entities = await strapi.services.identity.find(ctx.query);
    }

    return entities.map((entity) =>
      sanitizeEntity(entity, { model: strapi.models.identity })
    );
  },

  /**
   * Retrieve the record.
   *
   * @return {Object}
   */

  async findObject(ctx) {
    const entity = await strapi.services.identity.find();
    return sanitizeEntity(entity, { model: strapi.models.identity });
  },

  /**
   * Retrieve a record.
   *
   * @return {Object}
   */

  async findOne(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services.identity.findOne({ id });
    return sanitizeEntity(entity, { model: strapi.models.identity });
  },

  /**
   * Count records.
   *
   * @return {Number}
   */

  count(ctx) {
    if (ctx.query._q) {
      return strapi.services.identity.countSearch(ctx.query);
    }
    return strapi.services.identity.count(ctx.query);
  },

  /**
   * Create a record.
   *
   * @return {Object}
   */

  async create(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.identity.create(data, { files });
    } else {
      entity = await strapi.services.identity.create(ctx.request.body);
    }
    return sanitizeEntity(entity, { model: strapi.models.identity });
  },

  /**
   * Update a record.
   *
   * @return {Object}
   */

  async update(ctx) {
    const { id } = ctx.params;

    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.identity.update({ id }, data, {
        files,
      });
    } else {
      entity = await strapi.services.identity.update({ id }, ctx.request.body);
    }

    return sanitizeEntity(entity, { model: strapi.models.identity });
  },

  /**
   * Delete a record.
   *
   * @return {Object}
   */

  async delete(ctx) {
    const { id } = ctx.params;

    const entity = await strapi.services.identity.delete({ id });
    return sanitizeEntity(entity, { model: strapi.models.identity });
  },

  /**
   * Update the record.
   *
   * @return {Object}
   */

  async updateOrCreate(ctx) {
    let entity;
    if (ctx.is("multipart")) {
      const { data, files } = parseMultipartData(ctx);
      entity = await strapi.services.identity.createOrUpdate(data, {
        files,
      });
    } else {
      entity = await strapi.services.identity.createOrUpdate(ctx.request.body);
    }

    return sanitizeEntity(entity, { model: strapi.models.identity });
  },
};
