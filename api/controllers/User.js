"use strict";

module.exports = {
  // GET /user
  signup: async (ctx) => {
    // Store the new user in database.
    const user = await User.create(ctx.query);

    // Send an email to validate his subscriptions.
    strapi.services.email.send(
      "welcome@mysite.com",
      user.email,
      "Welcome",
      "..."
    );

    // Send response to the server.
    ctx.send({
      ok: true,
    });
  },
};
