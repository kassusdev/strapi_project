"use strict";

const { isDraft } = require("strapi-utils").contentTypes;
const _ = require("lodash");

module.exports = {
  /**
   * Promise to fetch all records
   *
   * @return {Promise}
   */
  find(params, populate) {
    return strapi.query("identity").find(params, populate);
  },

  /**
   * Promise to fetch the record
   *
   * @return {Promise}
   */
  async findRecord(populate) {
    const results = await strapi
      .query("identity")
      .find({ _limit: 1 }, populate);
    return _.first(results) || null;
  },

  /**
   * Promise to fetch record
   *
   * @return {Promise}
   */

  findOne(params, populate) {
    return strapi.query("identity").findOne(params, populate);
  },

  /**
   * Promise to count record
   *
   * @return {Promise}
   */

  count(params) {
    return strapi.query("identity").count(params);
  },

  /**
   * Promise to add record
   *
   * @return {Promise}
   */

  async create(data, { files } = {}) {
    const Draft = isDraft(data, strapi.models.identity);
    const validData = await strapi.entityValidator.validateEntityCreation(
      strapi.models.identity,
      data,
      { Draft }
    );

    const entry = await strapi.query("identity").create(validData);

    if (files) {
      // automatically uploads the files based on the entry and the model
      await strapi.entityService.uploadFiles(entry, files, {
        model: "identity",
        // if you are using a plugin's model you will have to add the `source` key (source: 'users-permissions')
      });
      return this.findOne({ id: entry.id });
    }

    return entry;
  },

  /**
   * Promise to edit record
   *
   * @return {Promise}
   */

  async update(params, data, { files } = {}) {
    const existingEntry = await db.query("identity").findOne(params);

    const Draft = isDraft(existingEntry, strapi.models.identity);
    const validData = await strapi.entityValidator.validateEntityUpdate(
      strapi.models.identity,
      data,
      { Draft }
    );

    const entry = await strapi.query("identity").update(params, validData);

    if (files) {
      // automatically uploads the files based on the entry and the model
      await strapi.entityService.uploadFiles(entry, files, {
        model: "identity",
        // if you are using a plugin's model you will have to add the `source` key (source: 'users-permissions')
      });
      return this.findOne({ id: entry.id });
    }

    return entry;
  },

  /**
   * Promise to delete a record
   *
   * @return {Promise}
   */

  delete(params) {
    return strapi.query("identity").delete(params);
  },

  /**
   * Promise to add/update the record
   *
   * @return {Promise}
   */

  async createOrUpdate(data, { files } = {}) {
    const results = await strapi.query("identity").find({ _limit: 1 });
    const entity = _.first(results) || null;

    let entry;
    if (!entity) {
      entry = await strapi.query("identity").create(data);
    } else {
      entry = await strapi.query("identity").update({ id: entity.id }, data);
    }

    if (files) {
      // automatically uploads the files based on the entry and the model
      await strapi.entityService.uploadFiles(entry, files, {
        model: "identity",
        // if you are using a plugin's model you will have to add the `plugin` key (plugin: 'users-permissions')
      });
      return this.findOne({ id: entry.id });
    }

    return entry;
  },
};
